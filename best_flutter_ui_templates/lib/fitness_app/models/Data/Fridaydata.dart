class FridayData {
  FridayData({
    this.imagePath = '',
    this.titleTxt = '',
    this.startColor = '',
    this.endColor = '',
    this.meals,
    this.kacl = 0,
  });

  String imagePath;
  String titleTxt;
  String startColor;
  String endColor;
  List<String>? meals;
  int kacl;

  static List<FridayData> tabIconsList = <FridayData>[
    FridayData(
      imagePath: 'assets/fitness_app/breakfast.png',
      titleTxt: 'Breakfast',
      kacl: 525,
      meals: <String>['Aloo Anda Fry,', 'Paratha,', 'Chai'],
      startColor: '#FA7D82',
      endColor: '#FFB295',
    ),
    FridayData(
      imagePath: 'assets/fitness_app/snack.png',
      titleTxt: 'Snack',
      kacl: 100,
      meals: <String>['Chai', 'Green Tea'],
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    FridayData(
      imagePath: 'assets/fitness_app/lunch.png',
      titleTxt: 'Lunch',
      kacl: 602,
      meals: <String>['Chicken Biryani', '', ''],
      startColor: '#738AE6',
      endColor: '#5C5EDD',
    ),
    FridayData(
      imagePath: 'assets/fitness_app/snack.png',
      titleTxt: 'Snack',
      kacl: 100,
      meals: <String>['Chai', 'Green Tea'],
      startColor: '#FE95B6',
      endColor: '#FF5287',
    ),
    FridayData(
      imagePath: 'assets/fitness_app/dinner.png',
      titleTxt: 'Dinner',
      kacl: 100,
      meals: <String>['Mix Sabzi', ''],
      startColor: '#6F72CA',
      endColor: '#1E1466',
    ),

  ];
}
